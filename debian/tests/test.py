import sys
import SubnetTree
t = SubnetTree.SubnetTree()
t["10.1.0.0/16"] = "Network 1"
t["10.1.42.0/24"] = "Network 1, Subnet 42"
if not ("10.1.42.1" in t):
    sys.exit(1)
